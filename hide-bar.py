#!/usr/bin/env python

import i3ipc

i3 = i3ipc.Connection()

classes = ['Firefox', 'chromium', 'Sublime_text']

def on_workspace_focus(self, event):
	focused = i3.get_tree().find_focused()

	if focused.window_class in classes:
		i3.command('bar mode hide')
	else:
		i3.command('bar mode dock')

i3.on('workspace::focus', on_workspace_focus)

i3.main()